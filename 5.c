#include<stdio.h>

float demand(float p){
return(180-(4*p));//demand equation(q = a-bp)
}
float revenue(float p){
return demand(p)*(p);
}
float expense(float p){
return(500+(demand(p)*3));
}
float profit(float p){
return revenue(p)-expense(p);
}

int main (){
    float p,count1,count2,max=1;
    	printf("\tPrice\t""\tProfit\n" "\t(Rs.)""\t\t(Rs.)\n\n" );
	 	 for(p=0;p<=45;p++){	 
			printf("\t%.2f\t""\t%10.2f\n",p,profit(p));
			count1=profit(p);
			if (count1>max){
				max=count1;
				count2=p;
				}			
			}
       			printf("\n Highest profit that can earn= %.2f\n",max);
       			printf("\n The price should charge to gain the highest profit= Rs. %.2f\n",count2);
    return 0;
}
